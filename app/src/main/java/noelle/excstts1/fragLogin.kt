package noelle.excstts1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_frag_login.*

class fragLogin : Fragment() {

    private lateinit var listener: InputActionListener

    companion object {
        fun newInstance(listener: InputActionListener) : fragLogin {
            val fragment = fragLogin()
            fragment.listener = listener
            return fragment
        }
    }

    interface InputActionListener{
        fun getLogin(us: String, pw: String)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.activity_frag_login,
                container,
                false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnLogin.setOnClickListener{
            var usn = usernameinput.text.toString()
            var pwd = passwordinput.text.toString()
            listener.getLogin(usn, pwd)
        }
    }
}
