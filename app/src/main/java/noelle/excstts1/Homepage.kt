package noelle.excstts1

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_homepage.*

class Homepage : AppCompatActivity() {

    companion object {
        fun getStartIntent(
                context: Context,
                us: String
        ) = Intent(context, Homepage::class.java).apply {
            putExtra("user", us)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homepage)

        val user_login = intent.getStringExtra("user")
        containertext.text = "Welcome, $user_login"
    }
}
