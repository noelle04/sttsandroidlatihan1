package noelle.excstts1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_frag_message.*

class fragMessage : Fragment() {

    companion object {
        fun newInstance() = fragMessage()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.activity_frag_message,
                container,
                false
        )
    }

    fun cekLogin(tulisan: String) {
        errormessage.append("$tulisan\n")
    }
}
